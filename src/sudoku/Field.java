package sudoku;

/*
 * The representation of the game field. Holding all 
 * values. Also providing methods for controlling the board.
 */
public class Field {

	/*
	 * The field of the sudoku. The data structure
	 * holding all values. 
	 */
	private int[][] field;
	
	/*
	 * Length of a row or column
	 */
	private int length; 
	
	/*
	 * Upper limit of the value of a row or column
	 */
	private int limit;
	
	
	/*
	 * @Constructor
	 * Creates a field of a given size and provides
	 * every entry in the field with a 0, representing
	 * an empty entry
	 */
	public Field(int size){
		
		length = size;
		limit = 0;
		field = new int[size][size];
		
		for(int i = 0; i< size; i++){
			for(int j = 0; j < size; j++){
				field[i][j] =  0;
			}
			limit += i+1;
		}

	}
	
	/*
	 * Empties the game field. Iterates over all
	 * positions in the field and sets the corresponding value to zero.
	 */
	protected void EmptyField(){
		for(int i = 0; i< length; i++){
			for(int j = 0; j < length; j++){
				field[i][j] =  0;
			}
		}
		
	}
	
	/*
	 * While the sudoku cannot be solved, a new 
	 * position and value is randomly added, as long 
	 * as it does not violate the rules of the sudoku.
	 */
	protected void GenerateField( Graphics g){
		
		while(!isSolveable()){
			int x = (int) (Math.random() * length-1);
			System.out.println("x-value is " + x);
			int y = (int) (Math.random() * length-1);
			System.out.println("y-value is " + y);
			int val = (int) (Math.random() *length);
			System.out.println("value is " + val);
			if(rowCheck(y, val) && columnCheck(x, val)){
				setField(x, y, val);
			}
		}
		g.AddLabels(field);
	}
	
	/*
	 * Sets the value of the entry with the given
	 * coordinates.
	 */
	protected void setField(int x, int y, int val) {
		field[x][y] = val;
		
	}

	/*
	 * A method for finding out whether or not the
	 * current sudoku field can be solved. 
	 * 
	 * Currently only counting the number of entries.
	 */
	protected boolean isSolveable(){
		boolean solve = false;
		
		int count = 0;
		for(int i = 0; i< length; i++){
			for(int j = 0; j < length; j++){
				if(field[i][j] !=  0){
					count++;
				}
				
			}			
		}

		return (count > 4);
	}
	

	/*
	 * Verifies that the same number does not occur
	 * twice in a row as well as checks that 
	 * the total value of the row does not exceed
	 * the maximum value of a row or column
	 */
	protected boolean rowCheck(int y, int val){
		int track = 0;
		for(int x = 0; x < length; x++){
			track += field[x][y];
			if(field[x][y] == val){
				return false;
			}
		}
		return (track <= limit);
	}
	
	/*
	 * Verifies that the same number does not occur
	 * twice in a column as well as checks that 
	 * the total value of the column does not exceed
	 * the maximum value of a row or column
	 */
	protected boolean columnCheck(int x, int val){
		int track = 0;
		for(int y = 0; y < length; y++){
			track += field[x][y];
			if(field[x][y] == val){
				return false;
			}
		}
		return (track < limit);
	}

	/*
	 * Getter for the game field
	 */
	protected int[][] getField(){
		return field;
	}
}
