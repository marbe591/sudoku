package sudoku;

public class Sudoku {
	
	/*
	 * The variable holding the field and logic
	 */
	private Field field;
	
	/*
	 * The variable holding the graphics object,
	 * responsible for displaying the game
	 */
	private Graphics g;
	
	public Sudoku(){
		field = new Field(4);
		g = new Graphics(4, field);
	}
	

	public static void main(String[] args) {
		
		Sudoku sud = new Sudoku();
	
	}

}
