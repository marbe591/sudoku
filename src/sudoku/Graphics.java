package sudoku;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

public class Graphics extends JComponent implements ActionListener{


	/*
	 * The frame holding all graphic components
	 */
	private JFrame mainFrame;

	/*
	 * The left half of the GUI
	 */
	private JPanel leftCon;

	/*
	 * The right half of the GUI
	 */
	private Container rightCon;

	/*
	 * Button to generate a new sudoku
	 */
	private JButton createButton;

	/*
	 * Button to reset the sudoku field
	 */
	private JButton resetButton;

	/*
	 * Variable remembering the size of a row or column
	 */
	private int size;

	/*
	 * The Field that owns the graphic component
	 */
	private Field field;

	/*
	 * Creates a graphics object, takes the length of a 
	 * row or column and the Field-object it represents.
	 */
	protected Graphics(int size, Field field){

		this.size = size;
		this.field = field;

		mainFrame = new JFrame("Sudoku");
		mainFrame.setVisible(true);
		mainFrame.setSize(600, 400);
		mainFrame.setLayout(new GridLayout(1,2));
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		leftCon = new JPanel();
		rightCon = new Container();

		mainFrame.add(leftCon, 0);
		mainFrame.add(rightCon, 1);

		rightCon.setLayout(new GridLayout(size, size));

		createButton = new JButton("Create sudoku");
		resetButton = new JButton("Reset field");
		createButton.setVisible(true);
		resetButton.setVisible(true);
		leftCon.add(createButton);
		leftCon.add(resetButton);

		/*	
		EtchedBorder e = new EtchedBorder();

		for(int i= 0; i <= size; i++){
			for(int j = 0; j <= size; j++){
				JLabel lab = new JLabel("");
				lab.setBorder(e);
				lab.setHorizontalTextPosition(JLabel.CENTER);
				lab.setVerticalTextPosition(JLabel.CENTER);
				rightCon.add(lab, i+j);
			}
		}
		 */
		createButton.addActionListener(this);
		resetButton.addActionListener(this);
	}

	protected void AddLabels(int [][] entries){
		EtchedBorder e = new EtchedBorder();

		for(int i= 0; i < size; i++){
			for(int j = 0; j < size; j++){
				JLabel lab;
				if(entries [i][j] != 0){
					lab = new JLabel("" + entries[i][j]);
				}
				else{
					lab = new JLabel("");
				}
				lab.setBorder(e);
				lab.setHorizontalTextPosition(JLabel.CENTER);
				lab.setVerticalTextPosition(JLabel.CENTER);
				rightCon.add(lab, i+j);
				
			}
		}
		rightCon.revalidate();
	}


	/*
	 * Updates the entire field by values provided
	 * in the matrix passed as argument 
	 */
	public void UpdateField(int[][] entries){
		for(int i= 0; i< size; i++){
			for(int j=0; j < size; j++){
				if(entries [i][j] != 0){
					UpdateLabel(i,j,entries[i][j]);
				}
			}
		}

	}

	/*
	 * Updates a single label with its corresponding
	 * entry
	 */
	protected void UpdateLabel(int x, int y, int val){
		int pos = x + y;
		((JLabel) rightCon.getComponent(pos)).setText("" + val);


	}

	/*
	 * Method to clear out all entries from the graphic 
	 * display
	 */
	protected void ClearLabels(){
		rightCon.removeAll();
		rightCon.revalidate();
		rightCon.repaint();
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		Object b = e.getSource();

		if(b == createButton){
			//Call method to generate a new sudoku
			field.GenerateField(this);
		}
		else if(b == resetButton){
			field.EmptyField();
			ClearLabels();
		}
		else{
			System.out.println("You messed up hacking!");
		}

	}

}
